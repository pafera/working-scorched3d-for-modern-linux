# working-scorched3d-for-modern-linux

Scorched3d version hacked by Pafera Technologies that actually compiles on modern Linux distributions as of 2023 so that you can play the game instead of battling against compiler errors. ;)

![Screenshot 1](https://codeberg.org/pafera/working-scorched3d-for-modern-linux/raw/branch/main/screenshot1.webp)

![Screenshot 2](https://codeberg.org/pafera/working-scorched3d-for-modern-linux/raw/branch/main/screenshot2.webp)

# Introduction

My first computer game was GORILLA.BAS on my old IBM PS/2, which I quickly hacked for bigger explosions and more dying effects. ;) The game involved typing in angles and power to control a gorilla standing on top of a city in order to throw explosive bananas to kill other gorillas. No one ever asked exactly why these gorillas just stood in one place and waited to be hit, or where their endless supply of explosive bananas came from, or even how bananas could explode in the first place. It was simply a fun game to play with friends.

There were many games like GORILLA.BAS, the most popular of which was probably Earthworm Jim. My friends and I had many fun memories of using various explosive projectiles to kill worms on the SNES.

Scorched3D at (http://scorched3d.co.uk), along with Atanks, is one of the best such games for Linux. 

Unfortunately, it seems that the source code is so old that it's hard to find a working version of Scorched3D anymore for a modern Linux system.

Fortunately, open source projects can always be revived by nostaglic programmers. ;)

# Installation

For Debian, you'll want to clone this repository, switch to its directory, and type 

```
cd scorched
sudo apt install libsdl1.2-dev libsdl-net1.2-dev libwxgtk3.0-gtk3-dev libglew-dev libalut-dev libogg-dev libfftw3-dev libjpeg-dev libfreetype-dev
make -j4
sudo make install
```

or just run 

```
sh install.sh
```

to do the same thing.

Other Linux systems should be similar, although you'll have to tinker with the package names and perhaps run configure again.

If you need to run configure again, be sure to use

```
LIBS="-lfreetype" ./configure
```

since the old configure script misses `-lfreetype` in a couple of Makefiles, and I'm too lazy to go back and debug autoconf scripts.

# Binary Installation

Of course, I've also included a handy binary installation for Debian systems as well at 

(https://codeberg.org/pafera/working-scorched3d-for-modern-linux/raw/branch/main/scorched3d-debian-amd64.tar.7z)

If you download it and run 

```
tar xvjpf scorched3d-debian-amd64.tar.7z -C /
```

You'll be able to run `/usr/local/games/scorched3d/bin/scorched3d` to start the game.

# Help Give This Project Some Visibility

Most of us open source guys just throw stuff up on code repositories and run off to do other things, but if you find this project helpful, please give it a post on your favorite social media site or app. It'll boost this project's search engine rankings and introduce other interested players to this game.

Have fun, and let me know if you guys have any problems! I'm not the original author, but even after all of these years, I'm still an enthusiastic fan with talents for fixing things. ;)
